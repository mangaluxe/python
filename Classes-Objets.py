class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    # On utilise __init__() pour attribuer des valeurs au nom et âge. C'est l'équivalent du constructeur
    # self est une référence à l'instance actuelle de la classe et est utilisé pour accéder aux variables appartenant à la classe

    def myfunc(self):
        print("Nom nom est " + self.name)


p1 = Person("John", 36)

print(p1.name) # Affiche: John
print(p1.age) # Affiche: 36
print(p1.myfunc()) # Affiche: Nom nom est John

# Modifier une propriété de l'objet :
p1.age = 40
print(p1.age) # Affiche: 40

# Effacer une propriété :
del p1.age