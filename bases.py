# Pour connaître la version de Python installée : python --version

print("Bonjour !")

# ==================== Variables, print(), Concaténation, input() ====================

# Une variable peut contenir des caractères alpha-numériques, majuscules et underscore. Exemples: "votrenom", "votreNom", "votre_nom", "votrenom2", "_votre_nom", mais pas un tiret "-" ni commencer par un chiffre

age = 22 # int (un entier)
votre_nom = "Tom" # str (une chaine). On utilise des guillemets " ou '
adresse = """7 rue du ciel
59000 Lille""" # Une chaine sur plusieurs lignes, on utilise 3 guillemets """ ou '''

if age >= 18 :
    print("Bienvenue " + votre_nom + " ! Vous êtes un adulte. Vous habitez à" , adresse + ". Vous avez " + str(age) + " ans. L'année prochaine, vous aurez " + str(age+1) + " ans !") # Le , ou + c'est pour la concaténation. On ne peut pas concaténer chaine et entier, on a donc utilisé str() pour convertir un entier en chaine. (age + 1) c'est une addition, pas une concaténation.
    # Affiche: Bienvenue Tom ! Vous êtes un adulte. Vous habitez à 7 rue du ciel 59000 Lille. Vous avez 22 ans. L'année prochaine, vous aurez 23 ans !
else:
    print("Interdit aux mineurs !")

# ----- Concaténation avec , -----
    
print("Bonsoir Joe ! Vous avez" , 15 , "ans !") # Par contre, avec la virgule, il est possible de concaténer chaine et entier. Affiche: Bonsoir Joe ! Vous avez 15 ans !

# ----- input() : Boite de dialogue pour demander d'entrer une valeur -----

nom = input("Votre nom ? ") # Ouvre une boite de dialogue pour entrer une valeur. On récupère la valeur dans une variable
age = input("Votre âge ? ")
print("Bonsoir " + nom + " ! Vous avez " + str(age) + " ans !") # Affiche: Bonsoir Votre-Nom ! Vous avez XX ans !

# Autres façons d'écrire :
print(f"Bonsoir {nom} ! Vous avez {age} ans !") # Une autre façon d'écrire : on met f pour formaté, on met les variables entre accolades. Pas besoin de mettre str() pour convertir l'âge en nombre
print("Bonsoir %s ! Vous avez %s ans !" % (nom, age)) # Une autre façon d'écrire : %s

# ==================== Types de données (str, int, float, complex, bool, list, dict...) ====================

print(type(age)) # Affiche: int
print(type(votre_nom)) # str
print(type(2.6)) # float
print(type(True)) # bool
print(type(["Pomme", "Poire", "Fraise"])) # list
print(type({"name" : "Terry", "age" : 36})) # dict

print(int(2.8)) # Convertit en entier. On obtient: 2
print(int("3")) # Convertit une chaine en entier. On obtient: 3
print(float(2.8)) # 2.8
print(float("3")) # 3.0
print(str(3.0)) # Convertit en chaine. On obtient: "3.0"


# ==================== Opérateurs ====================

# + 	Addition 	x + y 	
# - 	Soustraction 	x - y 	
# * 	Multiplication 	x * y 	
# / 	Division 	x / y 	
# % 	Modulo 	x % y 	
# ** 	Exponentiel 	x ** y 	
# // 	Floor division 	x // y

# ----- Opérateurs de comparaison : -----

# == 	Egal 	x == y 	
# != 	Pas egal 	x != y 	
# > 	Supérieur 	x > y 	
# < 	Inférieur 	x < y 	
# >= 	Supérieur ou égal 	x >= y 	
# <= 	Inférieur ou égal 	x <= y

# &  	AND 	Sets each bit to 1 if both bits are 1
# | 	OR 	    Sets each bit to 1 if one of two bits is 1
# ^ 	XOR 	Sets each bit to 1 if only one of two bits is 1
# ~  	NOT 	Inverts all the bits

# is    	x is y 	
# is not 	x is not y
# in    	x in y 	
# not in 	x not in y

# ----- Opérateurs d'assignement : -----

# += 	x += 3 	x = x + 3 	
# -= 	x -= 3 	x = x - 3 	
# *= 	x *= 3 	x = x * 3 	
# /= 	x /= 3 	x = x / 3

# age = input("Votre âge ? ") # Pour saisir une donnée dans l'interpréteur de commande (permet de tester)
# age = int(age) # On transforme en entier, car une donnée saisie est une chaine par défaut

age = 22

if age >= 18 and age <= 25:
    print("Vous êtes un jeune adulte !") # Affiche ceci
else:
    print("Ce site est réservé aux jeunes adultes !")


# ==================== Condition (If ... Else) ====================

a = 200
b = 33
if b > a: # Si
    print("b est plus grand que a")
elif a == b: # Sinon si
    print("a et b sont égaux")
else: # Sinon
    print("a est plus grand que b") # Affiche ceci


# ==================== Liste ====================

tableau = ["Pomme", "Poire", "Fraise"]

print(tableau[0]) # Index 0 du tableau. Affiche: Pomme
print(tableau[1]) # Index 1 du tableau. Affiche: Poire
print("Longueur du tableau :" , len(tableau)) # Longueur du tableau. Affiche: Longueur du tableau : 3

# append :
ma_liste = [1, 2, 3]
ma_liste.append(4) # On ajoute 4 à la fin de la liste
print("Ma liste 1 :" , ma_liste) # Affiche: Ma liste 1 : [1, 2, 3, 4]

# insert :
ma_liste = ['a', 'b', 'd', 'e']
ma_liste.insert(2, 'c') # On insère 'c' à l'indice 2
print("Ma liste 2 :" , ma_liste) # Affiche: Ma liste 2 : ['a', 'b', 'c', 'd', 'e']


# ==================== Boucle ====================

# ----- Boucle while : -----

i = 1
while i < 6:
    print(i)
    i += 1 # Equivalent à i = i + 1
# La boucle continue "tant que la condition est vraie"
else:
    print("i n'est plus plus petit que 6, la boucle while s'arrête.")

# Affiche:
# 1
# 2
# 3
# 4
# 5
# i n'est plus plus petit que 6, la boucle while s'arrête.

# -----

password = ""
while not password == "azerty":
    password = input("Quel est le mot de passe ? ") # Ouvre une boite de dialogue pour demander une valeur. La boucle continue tant que le mot de passe n'est pas "azerty"

print("Mot de passe correct !")

# -----

nom = ""
while nom == "": # Tant que le nom est vide, la boucle continue
    nom = input("Quel est votre nom ? ") # Boite de dialogue

age = 0
while age == 0:
    age_str = input("Quel est votre âge ? ") # La valeur récupérée dans une boite de dialogue est toujours une chaine
    try:
        age = int(age_str) # On convertit cette variable en entier
    except: # Si erreur, affiche ceci :
        print("Erreur ! Entrez un nombre !")

print("Bonjour " + nom + " ! Vous avez " + str(age) + " ans !") # Affiche: Bonjour VotreNom ! Vous avez 40 ans !


# ----- Boucle for : -----

fruits = ["Pomme", "Poire", "Fraise"]
for x in fruits:
    print(x)
# Affiche:
# Pomme
# Poire
# Fraise

fruits = ["Kiwi", "Banane", "Cerise"]
for x in fruits:
    print(x)
    if x == "Banane":
        break
# Affiche:
# Kiwi
# Banane

chaine = "Bonjour"
for lettre in chaine:
    print(lettre)
# Affiche:
# B
# o
# n
# j
# o
# u
# r


# ==================== Fonctions utilisateur ====================

def ma_fonction(p): # x c'est le paramètre
	y = 3 * p + 2
	return y # return permet de récupérer la valeur en dehors de la fonction, sinon on ne pourra pas utiliser en dehors de la fonction

solution = ma_fonction(4) # Appel à la fonction avec 4 en paramètre : ce qui donne y = 3 * 4 + 2. On peut stocker la fonction dans une variable

print(solution) # 14


# ----- Exemple de fonction avec boite de dialogue et boucle -----

def demander_nom():
    nom_str = ""
    while nom_str == "": # Tant que le nom est vide, la boucle continue
        nom_str = input("Quel est votre nom ? ") # Boite de dialogue
    return nom_str # Pour récupérer le nom en dehors de la fonction, sinon on ne pourra pas utiliser en dehors de la fonction

def demander_age():
    age_int = 0
    while age_int == 0:
        age_str = input("Quel est votre âge ? ") # La valeur récupérée dans une boite de dialogue est toujours une chaine
        try:
            age_int = int(age_str) # On convertit cette variable en entier
        except: # Si erreur, affiche ceci :
            print("Erreur ! Entrez un nombre !")
    return age_int # Pour récupérer l'âge en dehors de la fonction, sinon on ne pourra pas utiliser en dehors de la fonction

nom = demander_nom() # Appel à la fonction demander_nom(), puis récupère dans la varaible nom
age = demander_age() # Appel à la fonction demander_age(), puis récupère dans la varaible age

print("Bonjour " + nom + " ! Vous avez " + str(age) + " ans !") # Affiche: Bonjour VotreNom ! Vous avez 40 ans !


# ----- Exemple de fonction avec paramètre(s) -----

def afficher_info(nom, age): # Fonction avec paramètres nom et age.
    # On peut mettre des valeurs par défaut pour les paramètres d'une fonction. Exemple: def afficher_info(nom, age = 0). Ensuite, on appelle la fonction comme ça: afficher_info("Tom")
    print("Bonjour " + nom + " ! Vous avez " + str(age) + " ans !")

    if age >= 18:
        print("Vous êtes majeur !")
    elif 12 <= age < 18: # Equivalent: elif age >= 12 and age < 18:
        print("Vous êtes adolescent !")
    elif age == 0 or age == 1:
        print("Vous êtes un bébé !")
    else:
        print("Vous êtes mineur !")

afficher_info("Tom", 40) # Affiche: Bonjour Tom ! Vous avez 40 ans ! Vous êtes majeur !
afficher_info("Nana", 15) # Affiche: Bonjour Nana ! Vous avez 15 ans ! Vous êtes adolescent !
afficher_info("Boubou", 8) # Affiche: Bonjour Boubou ! Vous avez 8 ans ! Vous êtes mineur !
afficher_info("Titi", 1) # Affiche: Bonjour Boubou ! Vous avez 1 ans ! Vous êtes un bébé !


# ----- Lambda (fonction anonyme) -----

# Un argument :
x = lambda a : a + 10
print(x(5)) # 15

# Deux arguments :
x = lambda a, b : a * b
print(x(5, 6)) # 30